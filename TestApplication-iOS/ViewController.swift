//
//  ViewController.swift
//  TestApplication-iOS
//
//  Created by Ted Lythgoe on 16/12/2016.
//  Copyright © 2016 dreamr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var titleLbl: UILabel!
    var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.addTitleLbl()
        self.addImageView()
        self.addConstraints()
    }
    
    func addTitleLbl() {
        self.titleLbl = UILabel()
        self.titleLbl.text = "Good Luck!"
        self.titleLbl.textColor = UIColor.black
        self.titleLbl.font = UIFont(name: "Pokemon-X-and-Y", size: 40)
        self.titleLbl.translatesAutoresizingMaskIntoConstraints = false
        self.titleLbl.textAlignment = .center
        self.view.addSubview(self.titleLbl)
    }
    
    func addImageView() {
        self.imageView = UIImageView(image: #imageLiteral(resourceName: "ash"))
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.imageView.contentMode = .scaleAspectFit
        self.view.addSubview(self.imageView)
    }
    
    func addConstraints() {
        let views: [String: AnyObject] = ["imageView": self.imageView, "titleLbl": self.titleLbl]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-80-[titleLbl(100)]", options: [], metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[titleLbl]|", options: [], metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[imageView(250)]|", options: [], metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[imageView]|", options: [], metrics: nil, views: views))
    }
}

